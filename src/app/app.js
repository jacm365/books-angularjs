//Deps
import angular from 'angular';
import ngRoute from 'angular-route';
import ngStorage from 'angular-local-storage';
import jQuery from 'jquery';
import 'popper.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//Styles
import '../style/app.css';
//Controllers
import loginCtrl from './controllers/login.ctrl';
import registerCtrl from './controllers/register.ctrl';
import homeCtrl from './controllers/home.ctrl';
//Services
import requestSrv from './services/request.srv';
import authSrv from './services/auth.srv';
//Directives
import './directives/paginator.dir';
import popup from "./directives/popup.directive";

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor(authSrv, $location) {
    this.url = 'https://github.com/preboot/angular-webpack';
    this.authSrv = authSrv;
    this.$location = $location;
  }
}

AppCtrl.$inject= ['authSrv', '$location'];

const MODULE_NAME = 'app';
const API_URL = 'http://localhost:80';

angular.module(MODULE_NAME, [ngRoute, ngStorage, 'angularUtils.directives.dirPagination'])
  .directive('app', app)
  .directive('popup', popup)
  .constant('apiUrl', API_URL)
  .controller('AppCtrl', AppCtrl)
  .controller('loginCtrl', loginCtrl)
  .controller('registerCtrl', registerCtrl)
  .controller('homeCtrl', homeCtrl)
  .service('requestSrv', requestSrv)
  .service('authSrv', authSrv)
  .config((localStorageServiceProvider) =>{
    localStorageServiceProvider.setPrefix('booksAngularjs');
  })
  .config(($routeProvider, $locationProvider) => {
    $routeProvider
      .when('/', {
        templateUrl : 'views/login.view.html',
        controller  : 'loginCtrl',
        title       : 'Log In',
        resolve     : {
          access: (authSrv) => authSrv.isAnonymous()
        }
      })
      .when('/register', {
        templateUrl : 'views/register.view.html',
        controller  : 'registerCtrl',
        title       : 'Register',
        resolve     : {
          access: (authSrv) => authSrv.isAnonymous()
        }
      })
      .when('/home', {
        templateUrl : 'views/home.view.html',
        controller  : 'homeCtrl',
        title       : 'Welcome',
        resolve     : {
          access: (authSrv) => authSrv.isAuthenticated()
        }
      })
      .when('/404', {
        templateUrl : 'views/404.view.html',
        title       : 'Not found'
      })
      .otherwise({
        redirectTo : '/404'
      })
    $locationProvider.html5Mode(true);
  })
  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
  }])
  .run(($rootScope, authSrv, $location) => {
    $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
      switch (rejection) {
        case authSrv.UNAUTHORIZED:
          $location.path('/')
          break;

        case authSrv.FORBIDDEN:
          $location.path('/home')
          break;

        default:
          $log.warn("$stateChangeError event catched");
          break;
      }
    });
  });

export default MODULE_NAME;
