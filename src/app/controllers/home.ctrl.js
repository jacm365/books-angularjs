import jQuery from "jquery";

class homeCtrl {
  constructor(requestSrv, ngStorage, authSrv) {
    this.requestSrv = requestSrv;
    this.ngStorage = ngStorage;
    this.authSrv = authSrv;
  }

  pageChanged(newPage) {
    this.getBooks(newPage);
  }

  getBooks(page) {
    this.requestSrv.get('/books/?page= ' + page, { Authorization: 'Bearer ' + this.authSrv.getCurrentToken() })
      .then((response) => {
        console.log('Success books');
        this.books = response.data.books.data;
        this.totalBooks = response.data.books.total;
        this.booksPerPage = response.data.books.per_page;
      })
      .catch((response) => {
        console.log('There was an error retrieving the books');
        console.log(response);
      });
  }

  check(action) {
    this.checkError = '';
    this.requestSrv.put({ action: action, isbn: this.isbn }, '/check', { Authorization: 'Bearer ' + this.authSrv.getCurrentToken() })
      .then((response) => {
        this.successTitle = 'Successful check ' + (action === 'CHECKIN' ? 'in' : 'out');
        this.successMessage = 'The book with ISBN ' + this.isbn + ' was successfully checked ' + (action === 'CHECKIN' ? 'in' : 'out');
        jQuery('#messagePopup').modal();
        this.getBooks(this.pagination.current);
      })
      .catch((response) => {
        console.log('There was an error checkin the book');
        console.log(response);
        this.checkError = response.data.message;
      });
  }

  $onInit() {
    this.books = [];
    this.totalBooks = 0;
    this.booksPerPage = 5;
    this.isbn = '';
    this.checkError = '';
    this.successMessage = '';
    this.successTitle = '';
    this.statusLabels = {
      AVAILABLE: 'Available',
      CHECKED_OUT: 'Checked Out'
    }
    this.pagination = {
      current: 1
    }
    this.getBooks(1);

  }
}
homeCtrl.$inject= ['requestSrv', 'localStorageService', 'authSrv'];
export default homeCtrl;
