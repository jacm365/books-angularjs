import authSrv from "../services/auth.srv";

class loginCtrl {
  constructor(authSrv) {
    this.authSrv = authSrv;
  }

  login() {
    if(this.form.$valid) {
      console.log(this.email);
      console.log(this.password);
      let credentials = {
        email: this.email,
        password: this.password
      }
      this.authSrv.login(credentials)
    }
  }

  $onInit() {
    this.email = '';
    this.password = '';
  }
}
loginCtrl.$inject= ['authSrv'];
export default loginCtrl;
