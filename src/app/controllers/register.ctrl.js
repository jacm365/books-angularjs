class registerCtrl {
  constructor(requestSrv, ngStorage, $location) {
    this.requestSrv = requestSrv;
    this.ngStorage = ngStorage;
    this.$location = $location;
  }

  register() {
    if(this.form.$valid) {
      let user = {
        name: this.name,
        email: this.email,
        password: this.password,
        password_confirmation: this.confirmation,
        date_of_birth: this.dob
      }
      this.requestSrv.post(user, '/users').then(
        (response) => {
          this.requestSrv.successMessage('User created successfully');
          this.ngStorage.set('token', response.data.token);
          this.ngStorage.set('user', response.data.user);
          this.$location.path('/home');
        }, (response) => {
          this.requestSrv.errorMessage(response);
          this.errorMessages = response.data.errors;
        });
    }
  }

  $onInit() {
    this.name = '';
    this.email = '';
    this.password = '';
    this.confirmation = '';
    this.dob = '';
    this.errorMessages = {};
  }
}
registerCtrl.$inject= ['requestSrv', 'localStorageService', '$location'];
export default registerCtrl;
