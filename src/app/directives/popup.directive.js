let popup = function() {
  return {
    template : require('../../public/partials/popup.partial.html'),
    replace : true,
    restrict: 'E',
    link: function (scope, element, attributes) {
      attributes.$observe('title', function(value) {
        scope.title = value;
      });
      attributes.$observe('message', function(value) {
        scope.message = value;
      });
    }
  }
}
export default popup;
