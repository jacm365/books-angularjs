class authSrv {

  constructor(requestSrv, ngStorage, $q, $location) {
    this.requestSrv = requestSrv;
    this.ngStorage = ngStorage;
    this.$q = $q;
    this.$location = $location;
    this.OK = 200;
    this.UNAUTHORIZED = 401;
    this.FORBIDDEN = 403;
  }

  login(credentials) {
    if(this.ngStorage.isSupported) {
      console.log('Local storage is supported');
    }
    this.requestSrv.post(credentials, '/login').then(
      (response) => {
        this.requestSrv.successMessage('User successfully logged in');
        this.ngStorage.set('token', response.data.token);
        this.ngStorage.set('user', response.data.user);
        this.$location.path('/home');
      }, (response) => {
        this.requestSrv.errorMessage(response);
    });
  }

  isAuthenticated() {
    return !!this.getCurrentToken() ? this.OK : this.$q.reject(this.UNAUTHORIZED);
  }

  isAnonymous() {
    return !this.getCurrentToken() ? this.OK : this.$q.reject(this.FORBIDDEN);
  }

  getCurrentToken() {
    return this.ngStorage.get('token');
  }

  logout() {
    console.log('logout');
    //delete from server
    this.requestSrv.delete('/logout', { Authorization: 'Bearer ' + this.getCurrentToken() });
    //delete user and token from localStorage
    this.ngStorage.remove('token');
    this.ngStorage.remove('user');
    //redirect to (/)
    this.$location.path('/');
  }

}
authSrv.$inject= ['requestSrv', 'localStorageService', '$q', '$location'];
export default authSrv;
