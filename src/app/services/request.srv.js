class requestSrv {
  constructor($http, apiUrl) {
    this.$http = $http;
    this.apiUrl = apiUrl + '/api';
    this.defaultHeaders = {
      Accept: 'application/json'
    };
  }

  get(queryString, headers) {
    return this.$http.get(this.apiUrl + queryString, { headers: this.mergeHeaders(headers) });
  }

  put(data, endpoint, headers) {
    return this.$http.put(this.apiUrl + endpoint, data, { headers: this.mergeHeaders(headers) });
  }

  post(data, endpoint) {
    return this.$http.post(this.apiUrl + endpoint, data);
  }

  delete(endpoint, headers) {
    return this.$http.delete(this.apiUrl + endpoint, { headers: this.mergeHeaders(headers) });
  }

  mergeHeaders(headers) {
    return Object.assign(this.defaultHeaders, headers);
  }

  errorMessage(message) {
    console.log('there was an error', message);
  }

  successMessage(message) {
    console.log('success message', message)
  }
}
requestSrv.$inject= ['$http', 'apiUrl'];
export default requestSrv;
