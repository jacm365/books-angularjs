# Books Angularjs #

This project contains an Angularjs app that consumes and API, has token authentication
as login, register, and home controllers.
## Tech Requirements

- Nodejs 10 or greater
- Local port **8081** should be free

### Tech Stack ###

- Angularjs 1.6
- Boostrap 4.6

### Setup Instructions

If you want to run this project locally please run this commands:

```shell
npm install
```
```shell
npm start
```
